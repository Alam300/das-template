import styled from "styled-components";

export const BasicTab = styled.div`
  position: relative;
  display: flex;
  padding: 14px 16px;
  font-weight: normal;
  text-align: center;
  float: left;
  border-bottom: 4px solid #f3f3f3;
  cursor: pointer;
  line-height: 17px;
  font-family: "Lato" !important;
  border-bottom-color: ${props => (props.isSelected ? "#2e4b7a" : "#f3f3f3;")};
`;

export const StyledArrowTab = styled.div`
  position: relative;
  display: flex;
  padding: 14px 16px;
  font-weight: normal;
  text-align: center;
  float: left;
  border-top: 1px solid #e3e3e3;
  border-bottom: 1px solid #e3e3e3;
  cursor: pointer;
  line-height: 17px;
  font-family: "Lato" !important;
  background-color: ${props => (props.isSelected ? "#2e4b7a" : "white")};
  color: ${props => (props.isSelected ? "white" : "inherit")};
  :hover {
    background-color: #2e4b7a;
    color: #fff;

    :after {
      border-left-color: #2e4b7a;
    }
  }
  :before {
    content: "";
    position: absolute;
    top: 0;
    left: 100%;
    z-index: 1;
    display: block;
    width: 0;
    height: 0;
    border-top: 23px solid transparent;
    border-bottom: 21px solid transparent;
    border-left: 14px solid transparent;
    margin-left: 1px;
    border-left-color: #d5d5d5;
  }
  :after {
    content: "";
    position: absolute;
    top: 0;
    left: 100%;
    z-index: 1;
    display: block;
    width: 0;
    height: 0;
    border-top: 23px solid transparent;
    border-bottom: 21px solid transparent;
    border-left: 14px solid transparent;
    border-left-color: ${props => (props.isSelected ? "#2e4b7a" : "white")};
  }
  div {
    padding: 0 12px 0 20px;
  }
`;
