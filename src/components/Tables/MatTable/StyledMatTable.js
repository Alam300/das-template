import styled from "styled-components";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import StyledDivider from "../../Divider";
import Flex from "../../Flex";

export const Wrap = styled(Flex)`
  margin-top: ${props => (props.mt ? props.mt + "rem" : "0")};
  margin-bottom: ${props => (props.mb ? props.mb + "rem" : "0")};
  margin-left: ${props => (props.ml ? props.ml + "rem" : "0")};
  margin-right: ${props => (props.mr ? props.mr + "rem" : "0")};
  padding-top: ${props => (props.pt ? props.pt + "rem" : "0")};
  padding-bottom: ${props => (props.pb ? props.pb + "rem" : "0")};
  padding-left: ${props => (props.pl ? props.pl + "rem" : "0")};
  padding-right: ${props => (props.pr ? props.pr + "rem" : "0")};
`;

export const StyledTableRow = styled(TableRow)`
  margin: 8px 0;
  :hover {
    box-shadow: 0 0 2px #f3f3f3;
  }
`;

export const StyledTableHeadCell = styled(TableCell)`
  color: rgb(170, 170, 170);
  font-weight: 600;
`;

export const Divider = styled(StyledDivider)`
  background-color: #f3f3f3;
  height: 4px;
  width: 100%;
`;
