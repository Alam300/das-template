import React from "react";
import { withRouter } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";

import StyledList from "../../components/List";
import { StyledListItemIcon, Divider, StyledListItem } from "./StyledSideNav";

const drawerWidth = 276;

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    }),
    backgroundColor: "#2e2a43",
    color: "rgba(255,255,255,.87)"
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar
  }
}));

const SideNav = ({ history, location }) => {
  const classes = useStyles();

  const navTo = route => {
    const { pathname } = location;

    history.push(`/${route}`, { from: pathname });
  };

  return (
    <Drawer
      variant="permanent"
      className={classes.drawer}
      classes={{
        paper: clsx([classes.drawerOpen])
      }}
    >
      <div className={classes.toolbar}>
        <h2 style={{ paddingLeft: 8 }}>TITLE</h2>
      </div>
      <Divider />
      <StyledList>
        {["Dashboard", "Inbox", "Reports", "Complaints"].map((text, index) => (
          <StyledListItem
            button
            key={text}
            onClick={() => navTo(text.toLowerCase())}
          >
            <StyledListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </StyledListItemIcon>
            <ListItemText primary={text} />
          </StyledListItem>
        ))}
      </StyledList>
      <StyledList>
        {["Settings"].map((text, index) => (
          <StyledListItem
            button
            key={text}
            onClick={() => navTo(text.toLowerCase())}
          >
            <StyledListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </StyledListItemIcon>
            <ListItemText primary={text} />
          </StyledListItem>
        ))}
      </StyledList>
    </Drawer>
  );
};

export default withRouter(SideNav);
