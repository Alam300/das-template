import React, { Fragment } from "react";
import styled from "styled-components";

import StyledButton from "../../components/Buttons";
import Flex from "../../components/Flex";

const Button = styled(StyledButton)`
  padding: 0.5rem;
`;

const Dashboard = ({ history, location }) => {
  const logoutHandler = () => {
    const { pathname } = location;

    localStorage.removeItem("token");
    history.push("/login", { from: pathname });
  };
  return (
    <Flex>
      <Button small="small" callBack={() => logoutHandler()}>
        Sign Out
      </Button>
    </Flex>
  );
};

export default Dashboard;
