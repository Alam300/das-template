import React, { Fragment } from "react";
import styled from "styled-components";

import Flex from "../../components/Flex";
import MatTable from "../../components/Tables/MatTable/index";

const Wrap = styled(Flex)`
  padding: 3rem 0;
  width: 100%;
`;

const Inbox = () => {
  return (
    <Wrap>
      <MatTable pl={4} pr={4} />
    </Wrap>
  );
};

export default Inbox;
