import React, { Fragment, useState } from "react";
import IOSSwitch from "../../components/Switch/IOSSwitch";
import {
  WithoutLabelCheckbox,
  WithLabelCheckbox
} from "../../components/FormFields/StyledCheckbox";
import Flex from "../../components/Flex";

const Complaints = () => {
  const [checked, setChecked] = useState(false);

  const handleChange = () => {
    setChecked(!checked);
  };

  return (
    <Fragment>
      <Flex row>
        <IOSSwitch checked={checked} onChange={handleChange} />
        <IOSSwitch disabled checked={false} onChange={handleChange} />
        <IOSSwitch checked={true} onChange={handleChange} />
        <IOSSwitch disabled checked={true} onChange={handleChange} />
      </Flex>
      <Flex row>
        <WithoutLabelCheckbox />
        <WithoutLabelCheckbox disabled />
        <WithoutLabelCheckbox checked={true} />
        <WithoutLabelCheckbox checked={true} disabled />
      </Flex>
      <Flex row>
        <WithLabelCheckbox
          color="red"
          label="Hello world"
          labelPlacement="start"
        />
        <WithLabelCheckbox
          color="red"
          label="Hello world"
          disabled
          labelPlacement="top"
        />
        <WithLabelCheckbox
          label="Hello world"
          checked
          labelPlacement="bottom"
        />
        <WithLabelCheckbox
          label="Hello world"
          disabled
          checked
          labelPlacement="end"
        />
      </Flex>
    </Fragment>
  );
};

export default Complaints;
