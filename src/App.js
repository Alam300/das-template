import React, { Fragment } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ThemeProvider } from "@material-ui/styles";
import { StylesProvider } from "@material-ui/core/styles";

import LoginLayout from "./layouts/login/LoginLayout";
import DashboardLayout from "./layouts/DashboardLayout";
import NoRoute from "./containers/ErrorPages/NoRoute";

import Protected from "./utils/Protected";
import { lightTheme } from "./utils/customTheme";

const App = () => (
  <ThemeProvider theme={lightTheme}>
    <StylesProvider injectFirst>
      <Router>
        <Switch>
          <Protected
            exact
            route="login"
            path="/login"
            component={LoginLayout}
          />
          <Protected route="dash" path="/" component={DashboardLayout} />
          <Route path="/*" component={NoRoute} />
        </Switch>
      </Router>
    </StylesProvider>
  </ThemeProvider>
);

export default App;
